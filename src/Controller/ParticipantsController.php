<?php

namespace App\Controller;

use Doctrine\Common\Collections\Expr\Value;
use PhpParser\Node\Stmt\While_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParticipantsController extends AbstractController
{
    /**
     * @Route("/participants", name="participants")
     */
    public function index(): Response
    {
        $session = $this->get('session');
        $liste = $session->get('liste');
        $total = 0;
        foreach ($liste as $key => $value) {
            $total += $value['montant'];
        }
        // début
        $nb = count($liste);
        $bmmoyen = $total / $nb;

        $DoitRecevoir = [];
        $DoitDonner = [];

        foreach ($liste as $key => $value) {
            if ($bmmoyen < $value["montant"]) {
                array_push($DoitRecevoir, ["prénom" => $value["prénom"], "montant" => $value["montant"] - $bmmoyen]);
            } elseif ($bmmoyen > $value["montant"]) {
                array_push($DoitDonner, ["prénom" => $value["prénom"], "montant" => $bmmoyen - $value["montant"]]);
            } elseif ($bmmoyen == $value["montant"]) {
            }
        }
        $rnb = count($DoitRecevoir);
        $dnb = count($DoitDonner);

        echo "Le reglement doit se passer ainsi<br>";

        while ($rnb > 0 & $dnb > 0) {
            $Arecevoir = $DoitRecevoir[$rnb - 1]["montant"];

            if ($Arecevoir >= $DoitDonner[$dnb - 1]["montant"]) {
                echo "" . $DoitDonner[$dnb - 1]["prénom"] . " doit " . $DoitDonner[$dnb - 1]["montant"] . "€ à " . $DoitRecevoir[$rnb - 1]["prénom"] . "<br>";
                $DoitRecevoir[$rnb - 1]["montant"] = $Arecevoir - $DoitDonner[$dnb - 1]["montant"];
                $dnb--;
            } else {
                echo "" . $DoitDonner[$dnb - 1]["prénom"] . " doit " . $Arecevoir . "€ à " . $DoitRecevoir[$rnb - 1]["prénom"] . "<br>";
                $DoitDonner[$dnb - 1]["montant"] = $DoitDonner[$dnb - 1]["montant"] - $Arecevoir;
                $rnb--;
            }
        }

        return $this->render('participants/index.html.twig', [
            'liste' => $session->get('liste')
        ]);
    }
}
