<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add("Prenom", TextType::class, ['label' => "Prénom "])
            ->add("Montant", IntegerType::class, ['label' => "Montant "])
            ->add("ajouter", SubmitType::class, ["label" => "Ajouter la personne"])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $session = $this->get('session');
            if (!$session->get('liste')) {
                $session->set('liste', []);
                $liste = $session->get('liste');
            } else {
                $liste = $session->get('liste');
            }
            array_push($liste, ["prénom" => $data['Prenom'], "montant" => $data["Montant"]]);
            $session->set('liste', $liste);
        }
        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
